import Vue from 'vue'
import Router from 'vue-router'

import SSH from '../components/SSH'
Vue.use(Router)

export default new Router({
    routes: [
        {
            path: "/",
            name: SSH,
            component: SSH
        }],
})

